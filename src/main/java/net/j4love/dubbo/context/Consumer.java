package net.j4love.dubbo.context;

import com.alibaba.dubbo.rpc.RpcContext;
import net.j4love.dubbo.quickstart.provider.DemoService;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * @author He Peng
 * @create 2017-05-16 17:10
 * @update 2017-05-16 17:10
 * @see
 */
public class Consumer {

    public static void main(String[] args) {

        ClassPathXmlApplicationContext applicationContext =
                new ClassPathXmlApplicationContext("classpath:dubbo/multicast/dubbo-consumer.xml");

        applicationContext.start();

        DemoService demoService = applicationContext.getBean(DemoService.class);

        String echo = demoService.echoWithCurrentTime("同学们好我叫小明.");
        System.err.println(echo);

        RpcContext rpcContext = RpcContext.getContext();
        boolean isConsumer = rpcContext.isConsumerSide();
        System.err.println("is consumer : " + isConsumer);

        String remoteHost = rpcContext.getRemoteHost();
        System.err.println("remote host : " + remoteHost);
        // 每次发起 rpc 调用上下文状态都会变化

    }
}
