package net.j4love.dubbo.test;

/**
 * @author he peng
 * @create 2018/5/17 15:38
 * @see
 */
public class Zoo {

    private String name;
    private String address;

    public String briefIntroduce() {
        return "Hello everyone Welcome to come " + name + " Zoo!";
    }


    public String getName() {
        return name;
    }

    public Zoo setName(String name) {
        this.name = name;
        return this;
    }

    public String getAddress() {
        return address;
    }

    public Zoo setAddress(String address) {
        this.address = address;
        return this;
    }

    @Override
    public String toString() {
        return "Zoo{" +
                "name='" + name + '\'' +
                ", address='" + address + '\'' +
                '}';
    }
}
