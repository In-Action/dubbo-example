package net.j4love.dubbo.test;

import org.junit.Test;

import java.io.InputStream;
import java.lang.management.ManagementFactory;
import java.lang.management.OperatingSystemMXBean;
import java.lang.management.RuntimeMXBean;

/**
 * @author he peng
 * @create 2018/5/21 13:43
 * @see
 */
public class ManagementFactoryTest {

    @Test
    public void getOperatingSystemMXBeanTest() throws Exception {
        OperatingSystemMXBean operatingSystemMXBean = ManagementFactory.getOperatingSystemMXBean();
        System.out.println("操作系统架构 = " + operatingSystemMXBean.getArch());
        System.out.println("可用于Java虚拟机的处理器数量 = " + operatingSystemMXBean.getAvailableProcessors());
        System.out.println("操作系统名称 = " + operatingSystemMXBean.getName());
        System.out.println("操作系统版本 = " + operatingSystemMXBean.getVersion());
        System.out.println("最后一分钟的系统负载平均值 = " + operatingSystemMXBean.getSystemLoadAverage());

        RuntimeMXBean runtime = ManagementFactory.getRuntimeMXBean();
    }

    @Test
    public void getCpuUsageTest() throws Exception {

        String procCmd = System.getenv("windir")
                + "//system32//wbem//wmic.exe process get Caption,CommandLine,"
                + "KernelModeTime,ReadOperationCount,ThreadCount,UserModeTime,WriteOperationCount";
        System.out.println(procCmd);
        Process process = Runtime.getRuntime().exec(procCmd);
        process.getOutputStream().close();
        InputStream inputStream = process.getInputStream();
        byte[] buf = new byte[1024 * 50];
        int len;
        StringBuilder builder = new StringBuilder();
        while ((len = inputStream.read(buf)) != -1) {
            builder.append(new String(buf , 0 , len));
        }
        System.out.println(builder.toString());
    }

}
