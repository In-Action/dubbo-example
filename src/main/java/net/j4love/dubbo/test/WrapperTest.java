package net.j4love.dubbo.test;

import com.alibaba.dubbo.common.bytecode.Wrapper;
import org.junit.Test;

import java.util.Arrays;

/**
 * @author he peng
 * @create 2018/5/17 15:35
 * @see
 */
public class WrapperTest {

    @Test
    public void makeWrapperTest() throws Exception {
        Wrapper zooWrapper = Wrapper.getWrapper(Zoo.class);
        System.out.println(Arrays.deepToString(zooWrapper.getMethodNames()));
        System.out.println(Arrays.deepToString(zooWrapper.getPropertyNames()));
        Zoo zoo = new Zoo();
        zoo.setName("ASX");
        System.out.println(zooWrapper.invokeMethod(zoo , "briefIntroduce" , new Class[]{} , new Object[]{}));
    }

/*    public Object invokeMethod(Object o, String n, Class[] p, Object[] v) throws java.lang.reflect.InvocationTargetException {
        net.j4love.dubbo.test.Zoo w;
        try {
            w = ((net.j4love.dubbo.test.Zoo) $1);
        } catch (Throwable e) {
            throw new IllegalArgumentException(e);
        }
        try {
            if ("toString".equals($2) && $3.length == 0) {
                return ($w) w.toString();
            }
            if ("getAddress".equals($2) && $3.length == 0) {
                return ($w) w.getAddress();
            }
            if ("getName".equals($2) && $3.length == 0) {
                return ($w) w.getName();
            }
            if ("setName".equals($2) && $3.length == 1) {
                return ($w) w.setName((java.lang.String) $4[0]);
            }
            if ("briefIntroduce".equals($2) && $3.length == 0) {
                return ($w) w.briefIntroduce();
            }
            if ("setAddress".equals($2) && $3.length == 1) {
                return ($w) w.setAddress((java.lang.String) $4[0]);
            }
        } catch (Throwable e) {
            throw new java.lang.reflect.InvocationTargetException(e);
        }
        throw new com.alibaba.dubbo.common.bytecode.NoSuchMethodException("Not found method \"" + $2 + "\" in class net.j4love.dubbo.test.Zoo.");
    }*/


//    public Object invokeMethod(Object o, String n, Class[] p, Object[] v) throws java.lang.reflect.InvocationTargetException {
//        net.j4love.dubbo.quickstart.provider.DemoService w;
//        try {
//            w = ((net.j4love.dubbo.quickstart.provider.DemoService) $1);
//        } catch (Throwable e) {
//            throw new IllegalArgumentException(e);
//        }
//        try {
//            if ("echoWithCurrentTime".equals($2) && $3.length == 1) {
//                return ($w) w.echoWithCurrentTime((java.lang.String) $4[0]);
//            }
//        } catch (Throwable e) {
//            throw new java.lang.reflect.InvocationTargetException(e);
//        }
//        throw new com.alibaba.dubbo.common.bytecode.NoSuchMethodException("Not found method \"" + $2 + "\" in class net.j4love.dubbo.quickstart.provider.DemoService.");
//    }
}
