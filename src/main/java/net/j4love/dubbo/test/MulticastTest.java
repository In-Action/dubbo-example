package net.j4love.dubbo.test;

import org.junit.Test;

import java.net.DatagramPacket;
import java.net.InetAddress;
import java.net.MulticastSocket;
import java.nio.charset.Charset;

/**
 * @author he peng
 * @create 2018/5/17 18:25
 * @see
 */
public class MulticastTest {

    @Test
    public void multicast1() throws Exception {

        MulticastSocket multicastSocket = new MulticastSocket(8989);
        multicastSocket.setBroadcast(true);
        multicastSocket.setTimeToLive(250);
        InetAddress group = InetAddress.getByName("224.5.6.7");
        multicastSocket.joinGroup(group);

        byte[] buf = new byte[1000];
        DatagramPacket recvPacket = new DatagramPacket(buf, buf.length);
        multicastSocket.receive(recvPacket);
        System.out.println("Receive Packet From IP : " + recvPacket.getAddress().getHostAddress()
                + " port : " + recvPacket.getPort() + " Msg : " + new String(recvPacket.getData() , Charset.forName("utf-8")));

        String msg = "hello everyone too";
        DatagramPacket datagramPacket = new DatagramPacket(msg.getBytes(), msg.length(),
                group, recvPacket.getPort());
        multicastSocket.send(datagramPacket);
    }


    @Test
    public void multicast2() throws Exception {

        MulticastSocket multicastSocket = new MulticastSocket(8989);
        multicastSocket.setBroadcast(true);
        multicastSocket.setTimeToLive(250);

        InetAddress group = InetAddress.getByName("224.5.6.7");
        multicastSocket.joinGroup(group);
        String msg = "hello everyone";
        DatagramPacket datagramPacket = new DatagramPacket(msg.getBytes(), msg.length(),
                group, 8989);
        multicastSocket.send(datagramPacket);

        byte[] buf = new byte[1000];
        DatagramPacket recvPacket = new DatagramPacket(buf, buf.length);
        multicastSocket.receive(recvPacket);
        System.out.println("Receive Packet From IP : " + recvPacket.getAddress().getHostAddress()
                + " port : " + recvPacket.getPort() + " Msg : " + new String(recvPacket.getData() , Charset.forName("utf-8")));
    }

    @Test
    public void multicast3() throws Exception {

        MulticastSocket multicastSocket = new MulticastSocket(8989);
        multicastSocket.setBroadcast(true);
        multicastSocket.setTimeToLive(250);

        InetAddress group = InetAddress.getByName("224.5.6.7");
        multicastSocket.joinGroup(group);
        byte[] buf = new byte[1000];
        DatagramPacket recvPacket = new DatagramPacket(buf, buf.length);
        while (true) {
            multicastSocket.receive(recvPacket);
            System.out.println("Receive Packet From IP : " + recvPacket.getAddress().getHostAddress()
                    + " port : " + recvPacket.getPort() + " Msg : " + new String(recvPacket.getData() , Charset.forName("utf-8")));
        }
    }
}
