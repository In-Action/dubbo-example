package net.j4love.dubbo.callback.consumer;

import net.j4love.dubbo.callback.api.CallbackListener;
import net.j4love.dubbo.callback.api.CallbackService;
import org.springframework.context.support.ClassPathXmlApplicationContext;


/**
 * @author He Peng
 * @create 2017-05-28 22:21
 * @update 2017-05-28 22:21
 * @updatedesc : 更新说明
 * @see
 */
public class Consumer {

    public static void main(String[] args) throws InterruptedException {

        ClassPathXmlApplicationContext application =
                new ClassPathXmlApplicationContext(
                        "classpath:dubbo/callback/dubbo-consumer.xml");

        application.start();

        CallbackService callSrv = application.getBean(CallbackService.class);

        callSrv.addListener("first", new CallbackListener() {
            @Override
            public void changed(String msg) {
                System.err.println("callback : " + msg);
            }
        });

        callSrv.addListener("second", new CallbackListener() {
            @Override
            public void changed(String msg) {
                System.err.println("callback : " + msg);
            }
        });

        Thread.sleep(5000);
    }
}
