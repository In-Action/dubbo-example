package net.j4love.dubbo.callback.api;

/**
 * @author He Peng
 * @create 2017-05-28 21:58
 * @update 2017-05-28 21:58
 * @updatedesc : 更新说明
 * @see
 */
public interface CallbackService {

    void addListener(String key,CallbackListener listener);
}
