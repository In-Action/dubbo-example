package net.j4love.dubbo.callback.api;


/**
 * @author He Peng
 * @create 2017-05-28 21:59
 * @update 2017-05-28 21:59
 * @updatedesc : 更新说明
 * @see
 */
public interface CallbackListener {

    void changed(String msg);
}
