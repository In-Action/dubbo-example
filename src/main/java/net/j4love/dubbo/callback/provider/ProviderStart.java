package net.j4love.dubbo.callback.provider;

import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.io.IOException;

/**
 * @author He Peng
 * @create 2017-05-28 22:26
 * @update 2017-05-28 22:26
 * @updatedesc : 更新说明
 * @see
 */
public class ProviderStart {

    public static void main(String[] args) throws IOException {

        ClassPathXmlApplicationContext application =
                new ClassPathXmlApplicationContext("classpath:dubbo/callback/dubbo-provider.xml");
        application.start();
        System.in.read();
    }
}
