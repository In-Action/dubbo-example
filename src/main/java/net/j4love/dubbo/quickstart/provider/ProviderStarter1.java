package net.j4love.dubbo.quickstart.provider;

import com.alibaba.dubbo.config.spring.ServiceBean;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.io.IOException;
import java.util.Map;
import java.util.Objects;

/**
 * @author He Peng
 * @create 2017-05-16 17:10
 * @update 2017-05-16 17:10
 * @see
 */
public class ProviderStarter1 {

    public static void main(String[] args) throws IOException {
        ClassPathXmlApplicationContext application =
                new ClassPathXmlApplicationContext("classpath:dubbo/multicast/dubbo-provider-1.xml");
        application.start();
        Map<String, ServiceBean> serviceBeanMap = application.getBeansOfType(ServiceBean.class);
        if (Objects.nonNull(serviceBeanMap) && serviceBeanMap.size() > 0) {
            serviceBeanMap.forEach(
                    (key , serviceBean) ->
                            System.out.println("Dubbo Service Interface ==> "
                                    + key + "\r\n" + "Service ==> " + serviceBean));
        }
        System.in.read();
    }
}
