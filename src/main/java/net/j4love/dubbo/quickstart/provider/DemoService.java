package net.j4love.dubbo.quickstart.provider;


/**
 * @author He Peng
 * @create 2017-05-16 16:56
 * @update 2017-05-16 16:56
 * @see
 */
public interface DemoService {

    String echoWithCurrentTime(String src);
}
