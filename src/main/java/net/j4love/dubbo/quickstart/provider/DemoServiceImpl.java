package net.j4love.dubbo.quickstart.provider;


import org.springframework.transaction.annotation.Transactional;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author He Peng
 * @create 2017-05-16 16:58
 * @update 2017-05-16 16:58
 * @see
 */

public class DemoServiceImpl implements DemoService {

    @Transactional
    @Override
    public String echoWithCurrentTime(String src) {

        Date now = new Date();
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-DD HH:mm:ss");
        return src + " " + dateFormat.format(now);
    }
}
