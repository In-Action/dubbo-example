package net.j4love.dubbo.quickstart.consumer;

import com.alibaba.dubbo.rpc.service.GenericService;
import net.j4love.dubbo.quickstart.provider.DemoService;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.util.Arrays;

/**
 * @author He Peng
 * @create 2017-05-16 17:10
 * @update 2017-05-16 17:10
 * @see
 */
public class Consumer {

    public static void main(String[] args) throws Exception {

        ClassPathXmlApplicationContext applicationContext =
                new ClassPathXmlApplicationContext("classpath:dubbo/multicast/dubbo-consumer.xml");

        applicationContext.start();

        DemoService demoService = applicationContext.getBean(DemoService.class);
        System.out.println("Interfaces Class : " + Arrays.deepToString(demoService.getClass().getInterfaces()));
        String echo = demoService.echoWithCurrentTime("同学们好我叫小明.");
        System.err.println(echo);

        // 泛化调用
//        GenericService genericService = applicationContext.getBean(GenericService.class);
//        Object obj = genericService.$invoke("echoWithCurrentTime", new String[]{"java.lang.String"}, new Object[]{"同学们好我叫小明."});
//        System.out.println(obj);
//        System.in.read();
    }
}
