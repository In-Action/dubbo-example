package net.j4love.dubbo.quickstart.echo;

import com.alibaba.dubbo.rpc.service.EchoService;
import net.j4love.dubbo.quickstart.provider.DemoService;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * @author He Peng
 * @create 2017-05-28 15:50
 * @update 2017-05-28 15:50
 * @updatedesc : 更新说明
 * @see
 */
public class EchoTest {


    // dubbo 回声测试
    public static void main(String[] args) {
        ClassPathXmlApplicationContext applicationContext =
                new ClassPathXmlApplicationContext("classpath:dubbo/multicast/dubbo-consumer.xml");

        applicationContext.start();

        EchoService echoService = (EchoService)applicationContext.getBean(DemoService.class);

        Object echo = echoService.$echo("hello server");
        System.err.println("echo : " + echo);
    }
}
