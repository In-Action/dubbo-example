package net.j4love.dubbo.zookeeper.consumer;

import net.j4love.dubbo.zookeeper.provider.DemoService;
import net.j4love.dubbo.exception.ServiceException;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * @author He Peng
 * @create 2017-05-16 17:10
 * @update 2017-05-16 17:10
 * @see
 */
public class Consumer {

    public static void main(String[] args) {

        ClassPathXmlApplicationContext applicationContext =
                new ClassPathXmlApplicationContext("classpath:dubbo/zookeeper/dubbo-consumer.xml");

        applicationContext.start();

        DemoService demoService = applicationContext.getBean(DemoService.class);

        try {
            String demo = demoService.demo(4);
        } catch (ServiceException e) {

            Throwable cause = e.getCause();
            String message = e.getMessage();

            cause.printStackTrace();
            System.err.println("cause : " + cause);
            System.err.println("message : " + message);

        } finally {
            System.exit(0);
        }

    }
}
