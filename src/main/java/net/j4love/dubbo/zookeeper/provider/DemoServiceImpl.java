package net.j4love.dubbo.zookeeper.provider;

import net.j4love.dubbo.exception.ServiceException;

import java.sql.SQLException;
import java.util.concurrent.TimeUnit;

/**
 * @author He Peng
 * @create 2017-05-16 16:58
 * @update 2017-05-16 16:58
 * @see
 */
public class DemoServiceImpl implements DemoService {

    @Override
    public String demo(Integer exCode) throws ServiceException {

        switch (exCode) {

            case -1 :
                return "这下你规避异常了:)";
            case 0 :
                IllegalArgumentException ex0 = new IllegalArgumentException(exCode + " 无效参数异常");
                throw new ServiceException(ex0.getMessage(),ex0);
            case 1 :
                SQLException ex1 = new SQLException("大家好我是SQL Exception");
                throw new ServiceException(ex1.getMessage(),ex1);
            case 2 :
                IllegalAccessException ex2 = new IllegalAccessException("无效访问异常我也很无奈啊");
                throw new ServiceException(ex2);
            case 3 :
                SecurityException ex3 = new SecurityException("安全异常");
                throw new ServiceException(ex3);
            case 4 :
                try {
                    TimeUnit.SECONDS.sleep(1500);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            default :
                throw new RuntimeException("runtime exception 我故意抛出的");
        }

    }
}
