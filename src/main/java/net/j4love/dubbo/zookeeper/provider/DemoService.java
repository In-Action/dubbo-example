package net.j4love.dubbo.zookeeper.provider;

import net.j4love.dubbo.exception.ServiceException;

/**
 * @author He Peng
 * @create 2017-05-16 16:56
 * @update 2017-05-16 16:56
 * @see
 */
public interface DemoService {

    String demo(Integer exCode) throws ServiceException;
}
