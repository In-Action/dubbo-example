package net.j4love.dubbo.serialize;

import java.io.Serializable;
import java.util.List;

/**
 * @author He Peng
 * @create 2017-05-29 0:15
 * @update 2017-05-29 0:15
 * @updatedesc : 更新说明
 * @see
 */
public class Student implements Serializable {

    private String name;
    private Integer age;
    private String gender;
    List<Hobby> hobbies;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Student student = (Student) o;

        if (!name.equals(student.name)) return false;
        if (!age.equals(student.age)) return false;
        if (!gender.equals(student.gender)) return false;
        return hobbies.equals(student.hobbies);

    }

    @Override
    public int hashCode() {
        int result = name.hashCode();
        result = 31 * result + age.hashCode();
        result = 31 * result + gender.hashCode();
        result = 31 * result + hobbies.hashCode();
        return result;
    }

    public String getName() {
        return name;
    }

    public Student setName(String name) {
        this.name = name;
        return this;
    }

    public Integer getAge() {
        return age;
    }

    public Student setAge(Integer age) {
        this.age = age;
        return this;
    }

    public String getGender() {
        return gender;
    }

    public Student setGender(String gender) {
        this.gender = gender;
        return this;
    }

    public List<Hobby> getHobbies() {
        return hobbies;
    }

    public Student setHobbies(List<Hobby> hobbies) {
        this.hobbies = hobbies;
        return this;
    }

    @Override
    public String toString() {
        return "Student{" +
                "hobbies=" + hobbies +
                ", gender=" + gender +
                ", age=" + age +
                ", name='" + name + '\'' +
                '}';
    }
}
