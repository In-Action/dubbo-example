package net.j4love.dubbo.serialize;

import java.io.Serializable;

/**
 * @author He Peng
 * @create 2017-05-29 0:19
 * @update 2017-05-29 0:19
 * @updatedesc : 更新说明
 * @see
 */
public class Hobby implements Serializable {

    private String desc;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Hobby hobby = (Hobby) o;

        return desc != null ? desc.equals(hobby.desc) : hobby.desc == null;

    }

    @Override
    public int hashCode() {
        return desc != null ? desc.hashCode() : 0;
    }

    public String getDesc() {
        return desc;
    }

    public Hobby setDesc(String desc) {
        this.desc = desc;
        return this;
    }

    @Override
    public String toString() {
        return "Hobby{" +
                "desc='" + desc + '\'' +
                '}';
    }
}
