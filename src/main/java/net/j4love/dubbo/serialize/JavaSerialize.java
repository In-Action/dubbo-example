package net.j4love.dubbo.serialize;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * @author He Peng
 * @create 2017-05-29 0:49
 * @update 2017-05-29 0:49
 * @updatedesc : 更新说明
 * @see
 */
public class JavaSerialize {

    public static void main(String[] args) throws IOException, ClassNotFoundException {

        Student student = new Student();
        List<Hobby> hobbies = new ArrayList<>();
        hobbies.add(new Hobby().setDesc("学英语"));
        hobbies.add(new Hobby().setDesc("玩游戏"));
        hobbies.add(new Hobby().setDesc("打篮球"));
        hobbies.add(new Hobby().setDesc("吐槽"));

        student.setName("小明")
                .setAge(12)
                .setGender("男")
                .setHobbies(hobbies);

        byte[] bytes = serialize(student);
        System.err.println("byte size : " + bytes.length); // 424

        Object obj = deserialize(bytes);

        if (student.equals(obj)) {
            System.err.println(obj);
        }

    }

    public static byte[] serialize(Object object) throws IOException {
        ByteArrayOutputStream byteOutStream = new ByteArrayOutputStream();
        ObjectOutputStream objectOutStream = new ObjectOutputStream(byteOutStream);
        objectOutStream.writeObject(object);
        return byteOutStream.toByteArray();
    }

    public static Object deserialize(byte[] bytes) throws IOException, ClassNotFoundException {
        ByteArrayInputStream byteInStream = new ByteArrayInputStream(bytes);
        ObjectInputStream objectInStream = new ObjectInputStream(byteInStream);
        return objectInStream.readObject();
    }
}
