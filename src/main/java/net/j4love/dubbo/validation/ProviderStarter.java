package net.j4love.dubbo.validation;

import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.io.IOException;

/**
 * @author He Peng
 * @create 2017-05-16 17:10
 * @update 2017-05-16 17:10
 * @see
 */
public class ProviderStarter {

    public static void main(String[] args) throws IOException {

        ClassPathXmlApplicationContext application =
                new ClassPathXmlApplicationContext(
                        "classpath:dubbo/multicast/dubbo-provider-1.xml");
        application.start();
        System.in.read();
    }
}
