package net.j4love.dubbo.validation;

/**
 * @author He Peng
 * @create 2017-06-18 21:37
 * @update 2017-06-18 21:37
 * @updatedesc : 更新说明
 * @see
 */
public interface ValidationService {

    void save(ValidationParameter parameter);

    void update(ValidationParameter parameter);
}
