package net.j4love.dubbo.validation;

import com.alibaba.dubbo.rpc.RpcException;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import java.util.Set;

/**
 * @author He Peng
 * @create 2017-06-18 21:43
 * @update 2017-06-18 21:43
 * @updatedesc : 更新说明
 * @see
 */
public class ValidationConsumer {

    public static void main(String[] args) {
        ClassPathXmlApplicationContext context =
                new ClassPathXmlApplicationContext(
                        "classpath:dubbo/multicast/dubbo-consumer.xml");
        context.start();
        ValidationService validationService = context.getBean(ValidationService.class);
        // Error
        try {
            ValidationParameter parameter = new ValidationParameter();
            validationService.save(parameter);
            System.out.println("Validation ERROR");
        } catch (RpcException e) { // 抛出的是RpcException
            ConstraintViolationException ve = (ConstraintViolationException) e.getCause(); // 里面嵌了一个ConstraintViolationException
            Set<ConstraintViolation<?>> violations = ve.getConstraintViolations(); // 可以拿到一个验证错误详细信息的集合
            System.err.println(violations);
        }
    }
}
