package net.j4love.dubbo.validation;

import org.springframework.stereotype.Service;

/**
 * @author He Peng
 * @create 2017-06-18 21:38
 * @update 2017-06-18 21:38
 * @updatedesc : 更新说明
 * @see
 */

@Service
public class ValidationServiceImpl implements ValidationService {

    @Override
    public void save(ValidationParameter parameter) {
        System.out.println(parameter);
    }

    @Override
    public void update(ValidationParameter parameter) {
        System.out.println(parameter);
    }
}
