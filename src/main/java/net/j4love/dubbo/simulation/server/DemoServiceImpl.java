package net.j4love.dubbo.simulation.server;

import net.j4love.dubbo.simulation.api.DemoService;
import org.springframework.util.Assert;

import java.util.regex.Pattern;

/**
 * @author He Peng
 * @create 2017-05-26 23:34
 * @update 2017-05-26 23:34
 * @updatedesc : 更新说明
 * @see
 */
public class DemoServiceImpl implements DemoService {

    private static Pattern uppercasePattern = Pattern.compile("\\p{Upper}*");

    @Override
    public String convertToUppercase(String src) {

        Assert.notNull(src,"param src must not be null.");
        src = src.trim();
        if (uppercasePattern.matcher(src).matches()) {
            return src;
        }
        return src.toUpperCase();
    }
}
