package net.j4love.dubbo.simulation.server;

import com.alibaba.fastjson.JSON;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * @author He Peng
 * @create 2017-05-26 23:41
 * @update 2017-05-26 23:41
 * @updatedesc : 更新说明
 * @see
 */
public class RpcServer {

    public static void main(String[] args) throws IOException {

        int port = 6666;
        ServerSocket serverSocket = new ServerSocket(port);
        DemoServiceImpl demoService = new DemoServiceImpl();
        System.out.println("The rpc server is start in port : " + port);
        try {

            while (true) {
                System.out.println("****************** receive rpc invoke ******************");
                Socket socket = serverSocket.accept();
                InputStream inStream = socket.getInputStream();
                BufferedReader bufReader =
                        new BufferedReader(new InputStreamReader(inStream));
                String text = bufReader.readLine();
                String param = JSON.parseObject(text, String.class);
                String retVal = demoService.convertToUppercase(param);

                PrintWriter out = new PrintWriter(socket.getOutputStream(),true);
                out.println(retVal);

                System.out.println("****************** rpc invoke success ******************");
            }
        } catch (Throwable ex) {
            ex.printStackTrace();
        }
    }
}
