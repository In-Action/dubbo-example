package net.j4love.dubbo.simulation.api;

/**
 * @author He Peng
 * @create 2017-05-26 23:29
 * @update 2017-05-26 23:29
 * @updatedesc : 更新说明
 * @see
 */
public interface DemoService {

    String convertToUppercase(String src);
}
