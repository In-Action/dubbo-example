package net.j4love.dubbo.simulation.client;


import net.j4love.dubbo.simulation.api.DemoService;

/**
 * @author He Peng
 * @create 2017-05-26 23:42
 * @update 2017-05-26 23:42
 * @updatedesc : 更新说明
 * @see
 */
public class RpcClient {

    public static void main(String[] args) {

        ProxyInvoker proxyInvoker = new ProxyInvoker("192.168.1.103", 6666);
        DemoService demoService = proxyInvoker.getProxy(DemoService.class);
        String retVal = demoService.convertToUppercase("i am king od the world");
        System.out.println(retVal);
    }
}
